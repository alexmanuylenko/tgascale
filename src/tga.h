#ifndef TGA_H
#define TGA_H

#include <stdint.h>
#include <vector>
#include <string>

enum TgaImageType {
    NoImageData = 0,
    UncompressedColorMapped = 1, // not supported
    UncompressedTrueColor = 2, // RGB and RGBA
    UncompressedGrayScale = 3,
    RLEColorMapped = 9, // not supported
    RLETrueColor = 10, // RGB and RGBA
    RLEGrayScale = 11,
};

struct TgaHeader {
    uint8_t imageIdLength;
    uint8_t colorMapType;
    uint8_t imageType;
    uint16_t colorMapFirstEntryIndex;
    uint16_t colorMapLength;
    uint8_t colorMapEntryBitsPerPixel;
    uint16_t xOrigin;
    uint16_t yOrigin;
    uint16_t width;
    uint16_t height;
    uint8_t bitsPerPixelDepth;
    uint8_t imageDescriptor; // bits 3-0 give the alpha channel depth, bits 5-4 give direction
};

class TgaImage {
public:
    TgaImage() : header(nullptr), imageData(nullptr), colorMapData(nullptr) {}
    bool Load(std::string fileName);
    bool Save(std::string fileName);
    bool Scale(float xScaleFactor, float yScaleFactor);
    void Clear();
    ~TgaImage();
private:
    TgaHeader* header;
    uint8_t* imageData;
    uint8_t* colorMapData;
};

#endif // !TGA_H
