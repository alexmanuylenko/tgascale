#include "tga.h"

bool TgaImage::Load(std::string fileName) {
	Clear();

	FILE* stream = std::fopen(fileName.c_str(), "rb");
	if (stream == nullptr) {
		return false;
	}

	header = new TgaHeader;	
	size_t bytesRead = std::fread(header, sizeof(TgaHeader), 1, stream);
	if (bytesRead == 0) {
		Clear();
		std::fclose(stream);
		return false;
	}

	uint8_t* imageIdBuffer = new uint8_t[header->imageIdLength];
	bytesRead = std::fread(imageIdBuffer, sizeof(uint8_t), header->imageIdLength, stream);
	delete [] imageIdBuffer;
	if (bytesRead == 0) {
		Clear();
		std::fclose(stream);
		return false;
	}

	imageData = new uint8_t[header->width * header->height * header->bitsPerPixelDepth / sizeof(uint8_t)];
	colorMapData = new uint8_t[header->colorMapLength * header->colorMapEntryBitsPerPixel / sizeof(uint8_t)];

	switch (header->imageType) {
		case UncompressedColorMapped: {
			// Read color map
			// Read uncompressed color-mapped image
			// TODO
		}
		break;

		case UncompressedTrueColor: {
			// Read uncompressed true-color RGB and RGBA image		
			// TODO
		}
		break;
		
		case UncompressedGrayScale: {
			// Read uncompressed grayscale image		
			// TODO
		}
		break;
		
		case RLEColorMapped: {
			// Read color map
			// Read RLE-compressed color-mapped image		
			// TODO
		}
		break;
		
		case RLETrueColor: {
			// Read RLE-compressed true-color image
			// TODO
		}
		break;
		
		case RLEGrayScale: {
			// Read RLE-compressed grayscale image		
			// TODO
		}
		break;

		case NoImageData:
		default: {
			// Clear, close stream and exit
			Clear();
			std::fclose(stream);
			return false;
		}
		break;
	}

	std::fclose(stream);
	return true;
}

bool TgaImage::Save(std::string fileName) {
	// TODO
	return false;
}

bool TgaImage::Scale(float xScaleFactor, float yScaleFactor) {
	// TODO
	return false;
}

void TgaImage::Clear() {
	if (header != nullptr) {
		delete header;
		header = nullptr;
	}
	if (imageData != nullptr) {
		delete [] imageData;
		imageData = nullptr;
	}
	if (colorMapData != nullptr) {
		delete [] colorMapData;
		colorMapData = nullptr;
	}
}

TgaImage::~TgaImage() {
	Clear();
}
